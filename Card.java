public class Card{
	private int rank;
	private String suit;

	public Card(int rank, String suit){
		this.rank = rank;
		this.suit = suit;
	}
	
	public int getRank(){
		return this.rank;
	}
	
	public String getSuit(){
		return this.suit;
	}
	
	public String toString(){
		String printRank = Integer.toString(rank);
		if(rank == 13)
			printRank = "king";
		if(rank == 12)
			printRank = "queen";
		if(rank == 11)
			printRank = "jack";
		if(rank == 1)
			printRank = "ace";
		return printRank + " of " +  suit;
	}
}