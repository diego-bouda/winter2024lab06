import java.util.Random;


public class Deck{
	int numOfCards;
	Card[] stack;
	Random rng;
	
	public Deck(){
		rng = new Random();
		int[] ranks = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
		String[] suits = new String[]{"hearts", "diamonds", "spades", "clubs"};
		this.stack = new Card[52];
		numOfCards = 52;
		
		int i = 0;
		for(int r : ranks){
			for(String s : suits){
				stack[i] = new Card(r, s);
				i++;
			}
		}
		numOfCards = i;
	}
	
	public int length(){
		return this.numOfCards;
	}
	
	public Card drawTopCard(){
		numOfCards--;
		return stack[numOfCards];
	}
	public String toString(){
		String cardsInDeck = "";
		
		for(Card c : this.stack){
			cardsInDeck += c.toString() + "\n";
		}
		return cardsInDeck;
	}
	
	public void shuffle(){
		for (int i = 0; i < numOfCards; i++) {
			int randomIndex = i + rng.nextInt(numOfCards - i);
            Card card = stack[randomIndex];
            stack[randomIndex] = stack[i];
            stack[i] = card;
        }
    }
		
}