public class GameManager{
		
		public Deck drawPile;
		public Card centerCard;
		public Card playerCard;
		
		public GameManager(){
			this.drawPile = new Deck();
			drawPile.shuffle();
			this.centerCard = this.drawPile.drawTopCard();
			this.playerCard = this.drawPile.drawTopCard();
		}
		
		public String toString(){
			return "Center Card: " + this.centerCard.toString() + "\n" 
					+ "Player card: " + this.playerCard.toString();
		}
		
		public void dealCards(){
			this.drawPile.shuffle();
			this.centerCard = this.drawPile.drawTopCard();
			this.playerCard = this.drawPile.drawTopCard();
		}
		
		public int getNumberOfCards(){
			return this.drawPile.length();
		}
		
		public int calculatePoints(){
			int points = 0;
			
			if(centerCard.getRank() == playerCard.getRank())
				points += 4;
			if(centerCard.getSuit().equals(playerCard.getSuit()))
				points += 2;
			else
				points -= 1;
			
			return points; 
		}
		
}