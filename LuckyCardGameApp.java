import java.util.Scanner;

public class LuckyCardGameApp{
	public static void main (String[] args){
		
		/*Deck deck = new Deck();
		deck.shuffle();
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Welcome to the Lucky Card Game!");
        System.out.print("How many cards do you wish to remove? ");

        int cardsToRemove = scanner.nextInt();

        System.out.println("Initial number of cards in the deck: " + deck.length());
		
        for (int i = 0; i < cardsToRemove; i++) {
            deck.drawTopCard();
        }
		
        System.out.println("Number of cards after removal: " + deck.length());

        deck.shuffle();
		
        System.out.println("Shuffled deck:");
        System.out.println(deck.toString());
		
        scanner.close();*/
		
		GameManager manager = new GameManager();
		int totalPoints = 0;
		int round = 0;
		
		System.out.println("Welcome to the Lucky Card Game!");
		
		while(manager.drawPile.length() > 1 && totalPoints < 5){
			round += 1;
			System.out.println("Round: " + round);
			System.out.println(manager.toString());
			totalPoints += manager.calculatePoints();
			System.out.println("Your points after the round: " + totalPoints);
			manager.dealCards();
		}
		
		if(totalPoints < 5){
			System.out.println("You lost the game!");
		}else{
			System.out.println("You won the game!");
		}
	}
}